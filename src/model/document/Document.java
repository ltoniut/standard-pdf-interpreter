package model.document;
import java.util.List;

import model.template.Template;

public class Document {
	private Template template;

	public Template getTemplate() {
		return template;
	}
	
	private List<Field> Fields;
	
	public String FindValue(String a) {
		String value = "";
		
		for (Field item : Fields) {
			if (item.getName() == a) {
				value = item.getValue();
			}
		}
		
		return value;
	}
}
