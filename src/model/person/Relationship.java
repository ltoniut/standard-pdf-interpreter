package model.person;

public class Relationship {
	private Person a;
	private Person b;
	private String type;
	
	public Relationship (Person _a, Person _b, String _type) {
		this.a = _a;
		this.b = _b;
		this.type = _type; // Parent | Godparent | Spouse | 
	}
}
