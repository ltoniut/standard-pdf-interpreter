package model.person;

import java.util.List;
import model.document.Document;

public class Person {
	private int id;
	private List<Document> Documents;
	
	public String FindData(String query) {
		String value = "";
		
		for (Document doc : Documents) {
			if (doc.FindValue(query) != "") {
				value = doc.FindValue(query);
			}
		}
		
		return value;
	}

	public int getId() {
		return id;
	}
}
